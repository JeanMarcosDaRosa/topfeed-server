package controllers

import play.api.mvc.{Action, Controller}
import topfeed.server.security.AuthActionBuilder
import topfeed.server.config.di.InjectorProvider
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import topfeed.server.SessionService
import topfeed.server.security.HttpsOnlyActionCompositor

object SessionManagement extends Controller with AuthActionBuilder with InjectorProvider
with HttpsOnlyActionCompositor {
  val sessionService = injector.instance[SessionService]
  
  def login = httpsOnly{ Action {implicit request =>
    (for {
      username <- request.headers.get("Username")
      password <- request.headers.get("Password")
      sessionId <- sessionService.login(username, password)
    } yield sessionId) match {
      case None => Unauthorized
      case Some(sessionId) => Ok(sessionId)
    }
  }}
  
  def logout = ApiAuthAction {username => implicit request =>
    sessionService.logout(username)
    Ok("")
  }
}