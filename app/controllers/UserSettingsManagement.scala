package controllers

import play.api.mvc.Controller
import topfeed.server.security.AuthActionBuilder
import topfeed.server.config.di.InjectorProvider
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import topfeed.server.UsersService
import play.api.libs.json.{Json, JsError, JsSuccess}
import topfeed.server.json.{UserSettingsFormat, ActionJsonHelpers}
import topfeed.server.domain.UserSettings

object UserSettingsManagement extends Controller with AuthActionBuilder with InjectorProvider
with UserSettingsFormat with ActionJsonHelpers {
  
  val usersService = injector.instance[UsersService]
  
  def getUserSettings = ApiAuthAction {username => implicit request =>
    Ok(Json toJson usersService.userSettingsByUsername(username))
  }
  
  def setUserSettings = ApiAuthAction {username => implicit request =>
    withJsonBodyAs[UserSettings] { userSettings =>
      usersService.setUserSettings(username, userSettings)
      Ok
    }
  }
}