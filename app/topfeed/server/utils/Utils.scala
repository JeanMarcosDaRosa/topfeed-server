package topfeed.server.utils

object Utils {
  def inAnotherThread[T](f: => T) {
    new Thread(new Runnable {
      def run() {f}
    }).start
  }
}