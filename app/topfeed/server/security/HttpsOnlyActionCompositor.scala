package topfeed.server.security

import play.api.mvc._
import scala.concurrent.Future
import play.api.Logger

trait HttpsOnlyActionCompositor extends Results {
  def httpsOnly[A](action: Action[A]) = Action.async(action.parser) { request =>
    if(frontendHttpProxyReceivedHttps(request) || httpsPortIsBeingUsed(request))
      action(request)
    else
      Future.successful(Forbidden("Only HTTPS is allowed."))
  }
  
  private def frontendHttpProxyReceivedHttps[A](request: Request[A]) =
    request.headers.get("X-Forwarded-Proto") match {
      case Some("https") => true
      case _ => false
    }
  
  private def httpsPortIsBeingUsed[A](request: Request[A]) =
    Option(System.getProperty("https.port")).map {httpsPort =>
      request.host.split(":") contains httpsPort
    } getOrElse false
}