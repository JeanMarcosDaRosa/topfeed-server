package topfeed.server.security

import play.api.Play.current
import play.api._
import play.api.mvc._
import play.api.cache.Cache
import scala.concurrent.Future
import play.api.http.Status
import controllers.routes

trait AdminSecurityConstants {
  val admSessionCookieName = "adm_sess"
  val requestedUrlBeforeAdminLoginSessionName = "url-before-login"
}

object AdminAction extends ActionBuilder[Request] with Results
with HttpsOnlyActionCompositor with AdminSecurityConstants {
  def invokeBlock[A](request: Request[A], block: Request[A] => Future[SimpleResult]): Future[SimpleResult] = {
    Cache.getAs[String](admSessionCookieName).flatMap { adminSessionId =>
      request.cookies.get(admSessionCookieName).collect{
        case adminSessionId => block(request)
      }
    }.getOrElse{
      Future.successful(Redirect(routes.Admin.showLogin).withSession(
          request.session + (requestedUrlBeforeAdminLoginSessionName -> request.uri)))
    }
  }
  
  // Makes admin actions only accessible through https.
  override def composeAction[A](action: Action[A]): Action[A] = httpsOnly(action)
}