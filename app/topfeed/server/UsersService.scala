package topfeed.server


import topfeed.server.domain.{User, UserSettings, FeedChannelPriority}
import topfeed.server.dataaccess.UserRepository
import javax.inject.Inject
import org.neo4j.scala.DatabaseService
import org.neo4j.scala.Neo4jWrapper
import topfeed.server.domain.{FeedChannel, Rgb, BuildRgb}

// TODO: Rename to UserService.
// TODO: Stop using the withTx from userRepo.
class UsersService @Inject() (userRepo: UserRepository) {
  val defaultChannels = collection.mutable.Set(
      (FeedChannel("http://feeds.abcnews.com/abcnews/internationalheadlines"),BuildRgb.from256(32, 74, 135), FeedChannelPriority(0.5)),
      (FeedChannel("http://www.polygon.com/rss/index.xml"),                   BuildRgb.from256(78, 154, 6),  FeedChannelPriority(0.5)),
      (FeedChannel("http://www.gamespot.com/feeds/reviews/"),                 BuildRgb.from256(41, 80, 3),   FeedChannelPriority(0.5)),
      (FeedChannel("http://www.engadget.com/rss.xml"),                        BuildRgb.from256(164, 0, 0),   FeedChannelPriority(0.5)),
      (FeedChannel("http://feeds.gawker.com/io9/full"),                       BuildRgb.from256(187, 9, 219), FeedChannelPriority(0.5))
  )
  
  def addUser(user: User) {
    userRepo.withTx{implicit ds =>
      if(userRepo.userByUsernameOption(user.username).isDefined)
        throw new UsernameIsTakenException
      userRepo.addUser(user)
    }
  }
  
  def setUserSettings(username: String, userSettings: UserSettings) {
    userRepo.withTx {implicit ds =>
      userRepo.setUserSettings(username, userSettings)
    }
  }
  
  def userSettingsByUsername(username: String): UserSettings =
    userRepo.withTx {implicit ds =>
      var settings = userRepo.userSettingsByUsername(username)
      if(settings.channels.isEmpty) {
        settings = settings.copy(channels = defaultChannels)
        setUserSettings(username, settings)
      }
      settings
  	}
}

class UsernameIsTakenException extends Exception	// TODO: Move exceptions to a central place.