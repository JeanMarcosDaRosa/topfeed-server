package topfeed.server.json

import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError
import topfeed.server.domain.{UserSettings, FeedChannel, Rgb, FeedChannelPriority}
import scala.collection.mutable.Set

trait UserSettingsFormat extends UserSettingsReads with UserSettingsWrites {
  implicit val userSettingsFormat: Format[UserSettings] = Format(userSettingsReads, userSettingsWrites)
}

trait UserSettingsReads {
  import play.api.libs.json.Reads._
  
  implicit val userSettingsReads: Reads[UserSettings] =
    (__ \ "channels").read(list[(FeedChannel, Rgb, FeedChannelPriority)](
        (
          (__ \ "url").read[String] and
          (__ \ "color").read(
    	      (
                (__ \ "r").read[Double] and
                (__ \ "g").read[Double] and
                (__ \ "b").read[Double]
              )(Rgb)) and
          (__ \ "priority").read[Double]
        ) ((url, color, priority) => (FeedChannel(url), color, FeedChannelPriority(priority)))
      )
    ) map { chans: List[(FeedChannel, Rgb, FeedChannelPriority)] => UserSettings(Set() ++ chans) }
}

trait UserSettingsWrites {
  import play.api.libs.json.Writes._
  
  implicit val userSettingsWrites: Writes[UserSettings] =
    (__ \ "channels").write( traversableWrites[(FeedChannel, Rgb, FeedChannelPriority)] (
      (
          (__ \ "url").write[String] and
	      (__ \ "color").write (
	          ((__ \ "r").write[Double] and
	          (__ \ "g").write[Double] and
	          (__ \ "b").write[Double])
	          (unlift(Rgb.unapply))) and
	      (__ \ "priority").write[Double]
      ) ((t: (FeedChannel, Rgb, FeedChannelPriority)) => (t._1.url, t._2, t._3.value))
    ) ) contramap {_.channels}
}