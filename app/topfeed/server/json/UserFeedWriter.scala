package topfeed.server.json

import topfeed.server.domain.UserFeed
import play.api.libs.json._
import play.api.libs.json.Writes._
import play.api.libs.functional.syntax._
import topfeed.server.Vote

trait UserFeedWriter {
  implicit val userFeedWrites: Writes[UserFeed] =
    ((__ \ "url").write[String] and
    (__ \ "hasUserRead").write[Boolean] and
    (__ \ "userVote").write[Int] and
    (__ \ "upVotes").write[Int] and
    (__ \ "dnVotes").write[Int] and
    (__ \ "reads").write[Int] and
    (__ \ "creationTime").write[String]
    ) (uf => (uf.url, uf.isReadByUser, toInt(uf.vote),
        uf.numUpvotes, uf.numDnvotes, uf.numReads, uf.feedCreationTimeStr))
  
  private def toInt(v: Vote.Value) = v match {
      case Vote.Up => 1
      case Vote.None => 0
      case Vote.Down => -1
    }
}