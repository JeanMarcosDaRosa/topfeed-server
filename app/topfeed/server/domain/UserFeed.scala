package topfeed.server.domain

import topfeed.server.Vote

case class UserFeed (
  url: String,
  isReadByUser: Boolean,
  vote: Vote.Value,
  numUpvotes: Int,
  numDnvotes: Int,
  numReads: Int,
  feedCreationTimeStr: String
)