package topfeed.server.domain

import com.github.nscala_time.time.Imports._

case class FavoriteFeed(
    var url: String,
    var channelUrl: String,
    var title: String,
    var description: String,
    var creationDateStr: String = DateTime.now.toString())