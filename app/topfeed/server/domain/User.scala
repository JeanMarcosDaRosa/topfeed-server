package topfeed.server.domain

import com.github.nscala_time.time.Imports._


// TODO: Add registration date.
case class User(
    username: String,
    hashedPass: String,
    registrationTimeStr: String = DateTime.now.toString)
