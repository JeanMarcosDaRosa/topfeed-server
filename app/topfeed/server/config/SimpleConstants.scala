package topfeed.server.config

trait SimpleConstants {
  def appName = "TopFeed"
  def appBaseUrl = "http://localhost:9000"	// TODO: This url is actually defined at runtime.
}