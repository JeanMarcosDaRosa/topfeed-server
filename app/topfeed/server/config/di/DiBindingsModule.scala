package topfeed.server.config.di

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import javax.inject.Singleton
import topfeed.server.{UsersService, LoginSession, SessionService}
import topfeed.server.dataaccess.UserRepository
import org.neo4j.scala.DatabaseService
import net.tanesha.recaptcha.{ReCaptcha, ReCaptchaFactory}
import com.google.inject.name.Names
import topfeed.server.domain.{User, FeedChannel, Feed, FavoriteFeed}
import topfeed.server.dataaccess.dao._

/* The dependency injection configuration. Gets its DatabaseService as a parameter, since for instance
 * both production and testing would want to use DI, but with different data sources.
 */
class DiBindingsModule(val ds: DatabaseService) extends AbstractModule with ScalaModule
with DaoMakerProvider with UserDaoProvider with FeedChannelDaoProvider with LoginSessionDaoProvider
with FeedDaoProvider with FavoriteFeedDaoProvider {
  
  def recaptchaPublicKey = """6LfuKOQSAAAAAHcS21j3_L7mEkXtY_DWDYCmWOzo"""
  def recaptchaPrivateKey = """6LfuKOQSAAAAAL80jPB693wluA3Jk1ZbMVHA6Hcf"""
  
  def configure {
    bind[UsersService].in[Singleton]
    bind[UserRepository].in[Singleton]
    bind[SessionService].in[Singleton]
    bind[DatabaseService].toInstance(ds)
    bind[String].annotatedWith(Names.named("RecaptchaPublicKey")).toInstance(recaptchaPublicKey)
    bind[ReCaptcha].toInstance(ReCaptchaFactory.newSecureReCaptcha(recaptchaPublicKey, recaptchaPrivateKey, false))
    bind[Dao[User]].toInstance(userDao)
    bind[Dao[FeedChannel]].toInstance(feedChannelDao)
    bind[Dao[LoginSession]].toInstance(loginSessionDao)
    bind[Dao[Feed]].toInstance(feedDao)
    bind[Dao[FavoriteFeed]].toInstance(favoriteFeedDao)
  }
}