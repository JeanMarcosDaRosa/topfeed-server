package topfeed.server.dataaccess

import org.neo4j.graphdb.Direction
import org.neo4j.scala.DatabaseService
import javax.inject.Inject
import topfeed.server.domain.{User, UserSettings, FeedChannel, Rgb, FeedChannelPriority}
import org.neo4j.scala.Neo4jWrapper
import topfeed.server.dataaccess.dao.Dao
import org.neo4j.graphdb.Node
import collection.JavaConverters._
import collection.mutable
import topfeed.server.domain.FeedChannel
import topfeed.server.dataaccess.dao.NoSuchEntityError

// TODO: Move to a repositories package.
class UserRepository @Inject() (val ds: DatabaseService, val userDao: Dao[User], val channelDao: Dao[FeedChannel]) extends Neo4jWrapper {
  def userByUsername(username: String): User =
    userByUsernameOption(username) match {
      case None => throw new NoSuchEntityError(s"User with username = $username")
      case Some(user) => user
    }
  
  def updateUser(user: User) {
    val userNode = userDao.singleBy("username", user.username)
    Neo4jWrapper.serialize[Node](user, userNode)
  }
  
  def userByUsernameOption(username: String): Option[User] =
    userDao.singleOptionBy("username", username) flatMap {_.toCC[User]}
  
  def userNodeByUsername(username: String): Node = 
    userDao.singleBy("username", username)
  
  def addUser(user: User)(implicit ds: DatabaseService) {
    userDao.add(createNode(user))
  }
  
  def setUserSettings(username: String, userSettings: UserSettings)(implicit ds: DatabaseService) {
    val userNode = userDao.singleBy("username", username)
    deleteUserSettings(userNode)
    for((channel, color, priority) <- userSettings.channels) {
      val newRelationship = userNode --> RelationshipType.follows --> existingOrNewChannel(channel) <()
      newRelationship("color_r") = color.r
      newRelationship("color_b") = color.b
      newRelationship("color_g") = color.g
      newRelationship("priority") = priority.value
    }
  }
  
  def userSettingsByUsername(username: String): UserSettings = {
    val user = userDao.singleBy("username", username)
    val followedChannels = mutable.Set() ++ user.getRelationships(RelationshipType.follows).asScala map { rel =>
      val channel = rel.getEndNode.toCC[FeedChannel].get
      val channelColor = Rgb(rel("color_r").get, rel("color_g").get, rel("color_b").get)
      val channelPriority: Double = rel("priority").get
      (channel, channelColor, FeedChannelPriority(channelPriority))
    }
    UserSettings(followedChannels)
  }
  
  private def deleteUserSettings(userNode: Node) {
    for(followsRel <- userNode.getRelationships("follows").asScala) {
      val channelNode = followsRel.getEndNode
      followsRel.delete
      channelDao.deleteIfPossible(channelNode)
    }
  }
  
  private def existingOrNewChannel(channel: FeedChannel)(implicit ds: DatabaseService) =
    channelDao.singleOptionBy("url", channel.url).getOrElse(channelDao.add(channel))
  
  object RelationshipType {
    val follows = "follows"
  }
}