package topfeed.server.dataaccess.dao

import topfeed.server.domain.{User, FeedChannel, Feed, FavoriteFeed}
import topfeed.server.LoginSession
import org.neo4j.scala.DatabaseService

/* The providers in this file can be used both in the dependency injection module,
 * and in individual tests, where a dao is needed.
 */

trait DaoMakerProvider {
  def ds: DatabaseService
  val daoMaker = new DaoMaker(ds)
}

trait UserDaoProvider {this: DaoMakerProvider =>
  val userDao = daoMaker.makeForType[User]("User", indexKeys = List("username"))
}

trait FeedChannelDaoProvider {this: DaoMakerProvider =>
  val feedChannelDao = daoMaker.makeForType[FeedChannel]("FeedChannel", indexKeys = List("url"))
}

trait LoginSessionDaoProvider {this: DaoMakerProvider =>
  val loginSessionDao = daoMaker.makeForType[LoginSession]("LoginSession", List("sessionId", "username"))
}

trait FeedDaoProvider {this: DaoMakerProvider =>
  val feedDao = daoMaker.makeForType[Feed]("Feed", List("url"))
}

trait FavoriteFeedDaoProvider {this: DaoMakerProvider =>
  val favoriteFeedDao = daoMaker.makeForType[FavoriteFeed]("FavoriteFeed", List("url"))
}