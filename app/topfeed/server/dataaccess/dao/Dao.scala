package topfeed.server.dataaccess.dao

import org.neo4j.graphdb.Node
import org.neo4j.scala.DatabaseService

trait Dao[T] {
  def singleBy[ValT](indexKey: String, value: ValT): Node
  def singleOptionBy[ValT](indexKey: String, value: ValT): Option[Node]
  def singleCcBy[CC: Manifest, ValT](indexKey: String, value: ValT): CC
  def singleCcOptionBy[CC: Manifest, ValT](indexKey: String, value: ValT): Option[CC]
  def add(node: Node)(implicit ds: DatabaseService): Node
  def add(caseClass: T)(implicit ds: DatabaseService): Node
  def delete(node: Node)
  def deleteBy[ValT](indexKey: String, indexVal: ValT)
  def deleteIfPossible(node: Node)
}