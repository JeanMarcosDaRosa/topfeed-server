package topfeed.server

import topfeed.server.domain.User
import topfeed.server.utils.Pbkdf2Password
import javax.inject.Inject
import topfeed.server.dataaccess.UserRepository
import scala.util.Random
import topfeed.server.dataaccess.dao.Dao
import com.github.nscala_time.time.Imports._
import org.neo4j.scala.Neo4jWrapper
import org.neo4j.scala.DatabaseService

/* Does not use a SessionRepository, instead uses Daos directly, since the logic is too simple. */
class SessionService @Inject() (val ds: DatabaseService, userRepo: UserRepository, loginSessionDao: Dao[LoginSession]) extends Neo4jWrapper {
  /* Returns a session id on successful login, and None otherwise. */
  def login(username: String, password: String): Option[String] = {
    withTx {implicit ds =>
      userRepo.userByUsernameOption(username) flatMap { user =>
        if(! Pbkdf2Password.plaintextEqualsHashed(password, user.hashedPass)) None
        else {
          loginSessionDao.deleteBy("username", username)
          val sessionId = Random.alphanumeric.take(50).mkString
          loginSessionDao.add(LoginSession(sessionId, username))
          Some(sessionId)
        }
      }
    }
  }
  
  def usernameBySessionId(sessionId: String): Option[String] =
    loginSessionDao.singleCcOptionBy[LoginSession, String]("sessionId", sessionId) map {_.username}
  
  def logout(username: String) {
    withTx {implicit ds =>
      loginSessionDao.deleteBy("username", username)
    }
  }
}

case class LoginSession(sessionId: String, username: String, loginTime: String = DateTime.now.toString())