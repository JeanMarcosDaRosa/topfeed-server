package util

import org.specs2.specification.After

/* A specs2 testing scope that automatically closes the impermanent db after the test, causing it to vanish. */
trait ImpermanentGraphDatabaseServiceProviderScope extends ImpermanentGraphDatabaseServiceProvider with After {
  def after = {
    ds.gds.shutdown
  }
}