package util

import com.google.inject.Guice
import topfeed.server.config.di.DiBindingsModule

trait TestingInjectorProviderScope extends ImpermanentGraphDatabaseServiceProviderScope {  
  val injector = Guice.createInjector(new DiBindingsModule(ds))
}