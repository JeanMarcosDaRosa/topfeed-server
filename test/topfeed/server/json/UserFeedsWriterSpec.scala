package topfeed.server.json

import play.api.libs.json._
import org.specs2.mutable._
import org.specs2.specification.Scope
import topfeed.server.domain.{UserFeed, FeedChannel}
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import topfeed.server.Vote
import com.github.nscala_time.time.Imports._


class UserFeedsWriterSpec extends Specification {
  trait Environment extends Scope with UserFeedsWriter {
    val userFeed1 = UserFeed("http://url1", isReadByUser = true, vote = Vote.Up, numUpvotes = 2,
        numDnvotes = 1, numReads = 120, feedCreationTimeStr = DateTime.now.toString())
    val userFeed2 = UserFeed("http://url2", isReadByUser = false, vote = Vote.None, numUpvotes = 0,
        numDnvotes = 11, numReads = 123, feedCreationTimeStr = DateTime.now.toString())
    val userFeed3 = UserFeed("http://url3", isReadByUser = true, vote = Vote.Down, numUpvotes = 21,
        numDnvotes = 11, numReads = 1202, feedCreationTimeStr = DateTime.now.toString())
    
    val channel1 = FeedChannel("http://channel1")
    val channel2 = FeedChannel("http://channel2")
        
    val userFeeds = Map(
        channel1 -> List(userFeed1, userFeed2),
        channel2 -> List(userFeed3))
    
    val userFeedsJson = Json.arr(
      Json.obj(
        "channel" -> channel1.url,
        "feeds" -> Json.arr(
          Json.toJson(userFeed1),
          Json.toJson(userFeed2)
        )
      ), Json.obj(
        "channel" -> channel2.url,
        "feeds" -> Json.arr(
          Json.toJson(userFeed3)
        )
      )
    )
  }
  
  "UserFeedsWriterSpec (to json)" should {
    "Properly write a Map[FeedChannel, Iterable[UserFeed]] to json" in new Environment {
      Json.toJson(userFeeds) must_== userFeedsJson
    }
  }
}