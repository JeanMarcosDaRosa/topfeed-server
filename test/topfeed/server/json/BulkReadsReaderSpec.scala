package topfeed.server.json

import util.TestingInjectorProviderScope
import play.api.libs.json._
import org.specs2.mutable._
import org.specs2.specification.Scope

class BulkReadsReaderSpec extends Specification with BulkReadsReader {
  val chan1 = "http://channel1"
  val chan1feed1 = "http://ch1/url1"
  val chan1feed2 = "http://ch1/url2"
  val chan2 = "http://channel2"
  val chan2feed1 = "http://ch2/url1"
  
  val bulkReadsJson = Json.arr(
    Json.obj(
      "channel" -> chan1,
      "feeds" -> Json.arr(chan1feed1, chan1feed2)
    ), Json.obj(
      "channel" -> chan2,
      "feeds" -> Json.arr(chan2feed1)
    )
  )
  
  val bulkReads = Map(
    chan1 -> Set(chan1feed1, chan1feed2),
    chan2 -> Set(chan2feed1)
  )
  
  "BulkReadsReader" should {
    "Properly read the json of a bulk reads" in {
      Json.fromJson(bulkReadsJson).get must_== bulkReads
    }
  }
}