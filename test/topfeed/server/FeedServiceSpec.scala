package topfeed.server

import org.specs2.mutable._
import org.specs2.specification.Scope
import net.codingwell.scalaguice.InjectorExtensions.enrichInjector
import util.ImpermanentGraphDatabaseServiceProviderScope
import topfeed.server.domain.{User, Feed, FavoriteFeed, FeedChannel}
import topfeed.server.dataaccess.dao.Dao
import org.neo4j.scala.Neo4jWrapper
import util.TestingInjectorProviderScope
import topfeed.server.dataaccess.dao.NoSuchEntityError
import topfeed.server.dataaccess.UserRepository

class FeedServiceSpec extends Specification {
  trait Environment extends TestingInjectorProviderScope with Neo4jWrapper {
	val userService = injector.instance[UsersService]
    val channelDao = injector.instance[Dao[FeedChannel]]
	val feedDao = injector.instance[Dao[Feed]]
    val feedService = injector.instance[FeedService]
	val userDao = injector.instance[Dao[User]]
	
	val userRepo = injector.instance[UserRepository]	// TODO: Fix this! Look what we need userRepo in here for!
	
	val existingUser  = User("username", "somepass")
	val existingUser2 = User("another_user", "somepass")
	userService addUser existingUser
	userService addUser existingUser2
	
	val existingChannel1 = FeedChannel("http://existingChannel1")
	val existingChannel2 = FeedChannel("http://existingChannel2")
    withTx{implicit ds =>
      val ch1Node = channelDao.add(existingChannel1)
      val ch2Node = channelDao.add(existingChannel2)
      
      val usr1Node = userDao.singleBy("username", existingUser.username)
      usr1Node --> userRepo.RelationshipType.follows --> ch1Node
      usr1Node --> userRepo.RelationshipType.follows --> ch2Node
      
      val usr2Node = userDao.singleBy("username", existingUser2.username)
      usr2Node --> userRepo.RelationshipType.follows --> ch1Node
      usr2Node --> userRepo.RelationshipType.follows --> ch2Node
    }
	
    val bulkReads1 = Map(
        existingChannel1.url -> Set("http://ch1feedurl1", "http://ch1feedurl2"),
        existingChannel2.url -> Set("http://ch2feedurl1")
    )
    
    val bulkReads2 = Map(
        existingChannel1.url -> Set("http://ch1feedurl2", "http://ch1feedurl3")
    )
    
    implicit def bulkReads2ContainsAble(reads: Map[String, Set[String]]) = new {
      def containsRead(channelUrl: String, feedUrl: String) = reads.contains(channelUrl) && reads(channelUrl).contains(feedUrl)
    }
  }
  
  "FeedService" should {
    "Successfully upvote/dnvote/un-vote a feed of an existing channel only once per user" in new Environment {
      val feedUrl = "http://somefeedurl"
      
      feedService.vote(existingUser.username, existingChannel1.url, feedUrl, Vote.Up)
      val upvotedFeed = feedService.feedsByChannelUrl(existingChannel1.url).find{_.url == feedUrl}.get
      upvotedFeed.numUpvotes must_== 1      
      feedService.getUserVote(existingUser.username, feedUrl) must_== Vote.Up
      
      // Trying to upvote again.
      feedService.vote(existingUser.username, existingChannel1.url, feedUrl, Vote.Up) must throwA[FeedVotingException]
      val upvotedFeed2 = feedService.feedsByChannelUrl(existingChannel1.url).find{_.url == feedUrl}.get
      upvotedFeed2.numUpvotes must_== 1
      
      // Trying to downvote now.
      feedService.vote(existingUser.username, existingChannel1.url, feedUrl, Vote.Down)
      val downvotedFeed = feedService.feedsByChannelUrl(existingChannel1.url).find{_.url == feedUrl}.get
      downvotedFeed.numUpvotes.must_==(0) and downvotedFeed.numDnvotes.must_==(1)
      feedService.getUserVote(existingUser.username, feedUrl) must_== Vote.Down
      
      // Trying to remove vote.
      feedService.vote(existingUser.username, existingChannel1.url, feedUrl, Vote.None)
      val unvotedFeed = feedService.feedsByChannelUrl(existingChannel1.url).find{_.url == feedUrl}.get
      unvotedFeed.numUpvotes.must_==(0) and unvotedFeed.numDnvotes.must_==(0)
      feedService.getUserVote(existingUser.username, feedUrl) must_== Vote.None
    }
    
    "Throw an exception when trying to vote on a feed of a non-existant channel" in new Environment {
      feedService.vote(existingUser.username, "http://nonexistantchannel", "http://somefeedurl", Vote.Up) must throwA[NoSuchEntityError]
    }
    
    "Successfully favorite/unfavorite a feed of an existing channel" in new Environment {
      val feedUrl = "http://someurl"
      val favFeed = FavoriteFeed(feedUrl, "http://somechannel", "feed title", "feed description")
        
      feedService.favorite(existingUser.username, favFeed)
      feedService.favoriteFeedsByUsername(existingUser.username) must contain(favFeed)
      
      feedService.unfavorite(existingUser.username, feedUrl)
      feedService.favoriteFeedsByUsername(existingUser.username) must not contain(favFeed)
    }
    
    "Successfully batch-add the reads of an existing user only once" in new Environment {
      feedService.batchAddReads(existingUser.username, bulkReads1)
      
      for((channelUrl, feedUrls) <- bulkReads1; feedUrl <- feedUrls) {
        feedDao.singleCcBy[Feed, String]("url", feedUrl).numReads must_== 1
        feedService.hasUserReadFeed(existingUser.username, feedUrl) must_== true
      }
      
      feedService.batchAddReads(existingUser.username, bulkReads2)
      
      for((channelUrl, feedUrls) <- bulkReads2; feedUrl <- feedUrls) {
        feedDao.singleCcBy[Feed, String]("url", feedUrl).numReads must_== 1
        feedService.hasUserReadFeed(existingUser.username, feedUrl) must_== true
      }
      
      feedService.batchAddReads(existingUser2.username, bulkReads2)
      
      for((channelUrl, feedUrls) <- bulkReads2; feedUrl <- feedUrls) {
        feedDao.singleCcBy[Feed, String]("url", feedUrl).numReads must_== 2
        feedService.hasUserReadFeed(existingUser2.username, feedUrl) must_== true
      }
    }
    
    "return UserFeeds by username correctly" in new Environment {
      val feedUrls = (1 to 10).map{i => s"http://feedurl$i"}
      
      val user1sUpvotes = List(
        existingChannel1.url -> feedUrls(0),
        existingChannel1.url -> feedUrls(1),
        existingChannel2.url -> feedUrls(2)
      )
      
      val user1sDownvotes = List(
        existingChannel1.url -> feedUrls(3),
        existingChannel2.url -> feedUrls(4)
      )
      
      val user2sUpvotes = List(
        existingChannel1.url -> feedUrls(1),
        existingChannel2.url -> feedUrls(5)
      )
      
      val user2sDownvotes = List(
        existingChannel2.url -> feedUrls(2),
        existingChannel2.url -> feedUrls(6)
      )
      
      for((channelUrl, feedUrl) <- user1sUpvotes)
        feedService.vote(existingUser.username, channelUrl, feedUrl, Vote.Up)
      for((channelUrl, feedUrl) <- user1sDownvotes)
        feedService.vote(existingUser.username, channelUrl, feedUrl, Vote.Down)
      
      feedService.batchAddReads(existingUser.username, bulkReads1)
      feedService.batchAddReads(existingUser.username, bulkReads2)
        
      for((channelUrl, feedUrl) <- user2sUpvotes)
        feedService.vote(existingUser2.username, channelUrl, feedUrl, Vote.Up)
      for((channelUrl, feedUrl) <- user2sDownvotes)
        feedService.vote(existingUser2.username, channelUrl, feedUrl, Vote.Down)
        
      feedService.batchAddReads(existingUser2.username, bulkReads2)
      
      val userFeeds = feedService.userFeedsByUsername(existingUser.username)
      
      val pairsToCheck = user1sUpvotes ++ user1sDownvotes ++ user2sUpvotes ++ user2sDownvotes ++
        (for(
            bulkReads <- List(bulkReads1, bulkReads2);
            (channelUrl, feedUrls) <- bulkReads;
            feedUrl <- feedUrls) yield (channelUrl-> feedUrl))
	  
	  for((channelUrl, feedUrl) <-pairsToCheck) {
	    val userFeed = userFeeds(FeedChannel(channelUrl)).find{_.url == feedUrl}.get
    	userFeed.vote 		  must_== feedService.getUserVote(existingUser.username, feedUrl)
    	userFeed.isReadByUser must_== feedService.hasUserReadFeed(existingUser.username, feedUrl)
    	userFeed.numUpvotes   must_== feedService.feedsByChannelUrl(channelUrl).find{_.url == feedUrl}.get.numUpvotes
    	userFeed.numDnvotes	  must_== feedService.feedsByChannelUrl(channelUrl).find{_.url == feedUrl}.get.numDnvotes
	    userFeed.numReads 	  must_== feedService.feedsByChannelUrl(channelUrl).find{_.url == feedUrl}.get.numReads
	  }
    }
  }
}