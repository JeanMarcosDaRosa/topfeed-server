import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "TopFeedServer"
  val appVersion      = "1.0-SNAPSHOT"
  val neo4jVersion 	  = "1.9.1"

  val appDependencies = Seq(
    // Add your project dependencies here,
    //jdbc,
    //anorm,
    cache withSources,
    "com.github.nscala-time" %% "nscala-time" % "0.6.0" withSources,
    "org.neo4j" %% "neo4j-scala" % "0.2.0-M2-SBT-SNAPSHOT" withSources,	// Git clone and sbt publish-local from https://github.com/pouria-mellati/neo4j-scala.git
    "net.codingwell" %% "scala-guice" % "3.0.2" withSources,
    "org.apache.commons" % "commons-email" % "1.3.1" withSources,	// TODO: This one is no longer necessary.
    "commons-codec" % "commons-codec" % "1.8",
    "com.google.code.maven-play-plugin.net.tanesha.recaptcha4j" % "recaptcha4j" % "0.0.8" withSources,
    
    "org.mockito" % "mockito-all" % "1.9.5" % "test",
    "org.neo4j" % "neo4j-kernel" % neo4jVersion % "test" classifier "tests"	// This is the "test-jar" version of neo4j-kernel.
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    scalacOptions ++= Seq("-feature", "-language:reflectiveCalls")
  )
}
